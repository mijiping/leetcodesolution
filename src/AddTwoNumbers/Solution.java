package AddTwoNumbers;

import util.ListNode;

/**
 * Solution for problem 2: Add Two Number
 * url: https://leetcode.com/problems/add-two-numbers/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode ret = l1;
        int c = 0;
        int sum = 0;
        while(l1.next != null && l2.next != null) {
            sum = l1.val + l2.val + c;
            if (sum > 9) {
                c = 1;
                l1.val = sum - 10;
            } else {
                c = 0;
                l1.val = sum;
            }
            l1 = l1.next;
            l2 = l2.next;
        }
        l1.val += l2.val;
        if (l2.next != null) {
            l1.next = l2.next;
        }
        while (l1.next != null) {
            sum = c + l1.val;
            if (sum < 10) {
                l1.val = sum;
                break;
            } else {
                l1.val = sum - 10;
                c = 1;
            }
            l1 = l1.next;
        }

        if (l1.next == null) {
            sum = c + l1.val;
            if (sum > 9) {
                l1.val = sum - 10;
                ListNode end = new ListNode(1);
                l1.next = end;
            } else {
                l1.val = sum;
            }
        }
        return ret;
    }
}
