package BinaryTreeInorderTraversal;

import util.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Recursive solution for problem 94: Binary tree inorder traversal
 * url: http://leetcode.com/problems/binary-tree-inorder-traversal/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> list = new LinkedList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode element = root;
        while (!stack.isEmpty() || element != null) {
            while (element != null) {
                stack.push(element);
                element = element.left;
            }
            element = stack.pop();
            list.add(element.val);
            element = element.right;
        }
        return list;
    }
}
