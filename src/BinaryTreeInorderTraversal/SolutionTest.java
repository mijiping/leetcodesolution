package BinaryTreeInorderTraversal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sun.reflect.generics.tree.Tree;
import util.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SolutionTest {
    Solution solution = new Solution();

    @Test
    public void test00() {
        TreeNode root = new TreeNode(1);
        root.left = null;
        TreeNode right = new TreeNode(2);
        root.right = right;
        TreeNode left = new TreeNode(3);
        right.left = left;
        right.right = null;
        List<Integer> list = Arrays.asList(1,3,2);
        Assertions.assertEquals(list, solution.inorderTraversal(root));
    }

    @Test
    public void test01() {
        TreeNode root = new TreeNode(1);
        List<Integer> list = Arrays.asList(1);
        Assertions.assertEquals(list, solution.inorderTraversal(root));
    }

    @Test
    public void test02() {
        TreeNode root = new TreeNode(1);
        TreeNode left = new TreeNode(2);
        root.left = left;
        TreeNode right = new TreeNode(3);
        root.right = right;
        List<Integer> list = Arrays.asList(2,1,3);
        Assertions.assertEquals(list, solution.inorderTraversal(root));
    }
}
