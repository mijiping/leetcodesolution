package BinaryWatch;

import java.util.LinkedList;
import java.util.List;

/**
 * solution for problem 401： Binary Watch
 * url：http://leetcode.com/problems/binary-watch/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    public List<String> readBinaryWatch(int num) {
        List<String> list = new LinkedList<>();
        generateTime(num, 10, 0, list);
        return list;
    }

    private void generateTime(int num, int digit, int time, List<String> list) {
        if (num == 0) {
            String str = formatTime(time);
            if (str != null) {
                list.add(str);
            }
            return;
        }
        if (num == digit) {
            time += Math.pow(2, digit) - 1;
            String str = formatTime(time);
            if (str != null) {
                list.add(str);
            }
            return;
        }
        generateTime(num-1, digit-1, time + (1 << digit-1), list);
        generateTime(num, digit-1, time, list);
    }

    private String formatTime(int time) {
        int hour = time >> 6;
        int minutes = 0x3f & time;
        if (hour > 11 || minutes > 59) {
            return null;
        }
        String minute = String.valueOf(minutes);
        if (minutes < 10) {
            minute = "0" + minute;
        }
        return hour + ":" + minute;
    }
}
