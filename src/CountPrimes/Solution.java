package CountPrimes;

import java.util.Arrays;

/**
 * Solution for problem 204: count primes
 * url: http://leetcode.com/problems/count-primes/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public int countPrimes(int n) {
        boolean[] isPrime = new boolean[n];
        Arrays.fill(isPrime, true);
        for (int i = 2; i * i < n; i++) {
            if (!isPrime[i]) continue;
            for (int j = i * i; j < n; j += i) {
                isPrime[j] = false;
            }
        }
        int count = 0;
        for (int i = 2; i < n; i++) {
            if(isPrime[i]) {
                count++;
            }
        }
        return count;
    }
}
