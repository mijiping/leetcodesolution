package CountPrimes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SolutionTest {

    Solution solution;

    @BeforeEach
    public void init() {
        solution = new Solution();
    }

    @Test
    public void test() {
        assertAll(
                () -> assertEquals(0, solution.countPrimes(0)),
                () -> assertEquals(0, solution.countPrimes(1)),
                () -> assertEquals(0, solution.countPrimes(2)),
                () -> assertEquals(1, solution.countPrimes(3)),
                () -> assertEquals(2, solution.countPrimes(4)),
                () -> assertEquals(4, solution.countPrimes(10))
        );
    }
}
