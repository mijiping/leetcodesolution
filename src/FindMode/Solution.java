package FindMode;

import util.TreeNode;

import java.util.*;

/**
 * solution for problem 501: find mode in binary search tree
 * url: http://leetcode.com/problems/find-mode-in-binary-search-tree/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public int[] findMode(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        ArrayList<Integer> list = new ArrayList<>();
        int count = 1, maxCount = 1;
        boolean first = true;
        int preValue = 0;
        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            TreeNode node = stack.pop();
            if (first) {
                list.add(node.val);
                first = false;
            } else {
                if (node.val == preValue) {
                    count++;
                } else {
                    count = 1;
                }
                if (count == maxCount) {
                    list.add(node.val);
                }
                if (count > maxCount){
                    list.clear();
                    list.add(node.val);
                    maxCount = count;
                }
            }
            preValue = node.val;
            if (node.right != null) {
                root = node.right;
            }
        }
        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        TreeNode root = new TreeNode(1);
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(2);
        int[] a = solution.findMode(root);
        Arrays.stream(a).forEach(System.out::println);
    }
}
