package FindPeakElement;

/**
 * Solution for problem 162: Find Peak Element
 * url: https://leetcode.com/problems/find-peak-element/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public int findPeakElement(int[] nums) {
        int l = 0, r = nums.length - 1;
        while (l < r) {
            int m = (l + r) / 2;
            if (nums[m+1] > nums[m]) {
                l = m + 1;
            } else {
                r = m;
            }
        }
        return l;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        int[] nums1 = {1,2,3};
        System.out.println(s.findPeakElement(nums1));
        int[] nums2 = {1,2,3,2,1};
        System.out.println(s.findPeakElement(nums2));
        int[] nums3 = {1,2,3,2,1,2,3,2,1};
        System.out.println(s.findPeakElement(nums3));
    }
}
