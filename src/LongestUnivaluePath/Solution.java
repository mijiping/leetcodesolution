package LongestUnivaluePath;

import util.TreeNode;

/**
 * recursive solution for problem 687: Longest Univalue Path
 * url: https://leetcode.com/problems/longest-univalue-path/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    int longest = 0;
    public int longestUnivaluePath(TreeNode root) {
        helper(root, -1);
        return longest;
    }

    private int helper(TreeNode root, int prev) {
        if(root == null) {
            return 0;
        }
        int lVal = helper(root.left, root.val);
        int rVal = helper(root.right, root.val);
        longest = Math.max(longest, lVal+rVal);
        if(root.val == prev) {
            return Math.max(lVal, rVal) + 1;
        }
        return 0;
    }
}