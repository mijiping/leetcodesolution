package MiniSizeSubarraySum;

/**
 *  Solution for problem 209: Minimum Size Subarray Sum
 *  url: http://leetcode.com/problems/minimum-size-subarray-sum/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public int minSubArrayLen(int s, int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int sum = 0;
        int result = Integer.MAX_VALUE;
        int index = 0;
        for (int i = 0; i < nums.length; ++i) {
            sum += nums[i];
            if (sum >= s) {
                int size = i - index + 1;
                for (int j = index; j < i; j++) {
                    if (sum - nums[j] >= s) {
                        size--;
                        index++;
                        sum -= nums[j];
                    } else {
                        break;
                    }
                }
                if (size < result) {
                    result = size;
                }
            }

        }
        return sum < s ? 0 : result;
    }
}
