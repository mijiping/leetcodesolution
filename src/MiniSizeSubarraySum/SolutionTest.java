package MiniSizeSubarraySum;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SolutionTest {

    Solution solution;

    @BeforeEach
    public void init() {
        solution = new Solution();
    }

    @Test
    public void test() {
        int nums[] = {2,3,1,2,4,3};
        assertEquals(2, solution.minSubArrayLen(7, nums));
    }

    @Test
    public void test01() {
        int[] nums = {1,2,3,4,5};
        assertEquals(3, solution.minSubArrayLen(11, nums));
    }

    @Test
    public void test02() {
        int[] nums = {};
        assertEquals(0, solution.minSubArrayLen(11, nums));
    }

    @Test
    public void test03() {
        int[] nums = {1,2};
        assertEquals(0, solution.minSubArrayLen(11, nums));
    }

    @Test
    public void test04() {
        int[] nums = {1,2,3,4,5};
        assertEquals(5, solution.minSubArrayLen(15, nums));
    }
}
