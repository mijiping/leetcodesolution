package MinimumTimeDifference;

import java.util.Arrays;
import java.util.List;

/**
 *  Solution for problem 539: Minimum Time Difference
 *  url: http://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    public int findMinDifference(List<String> timePoints) {
        if (timePoints == null || timePoints.size() < 2) {
            return 0;
        }
        int[] times = new int[timePoints.size()];
        int index = 0;
        for (String element : timePoints) {
            int hour = (element.charAt(0) - '0') * 10 + element.charAt(1) - '0';
            int minutes = (element.charAt(3) - '0') * 10 + element.charAt(4) - '0';
            int time = hour * 60 + minutes;
            times[index++] = time;
        }
        Arrays.sort(times);
        int min = Integer.MAX_VALUE;
        int previous = times[0];
        for (int i = 1; i < times.length; i++) {
            if ( times[i] - previous < min) {
                min = times[i] - previous;
            }
            previous = times[i];
        }
        return Integer.min(min, 1440  + times[0] - times[times.length-1]);
    }
}
