package MinimumTimeDifference;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */

public class SolutionTest {

    Solution solution;
    List<String> list;

    @BeforeEach
    public void init() {
        solution = new Solution();
    }

    @Test
    public void test01() {
        list = Arrays.asList("23:59", "00:00");
        assertEquals(1, solution.findMinDifference(list));
    }
}
