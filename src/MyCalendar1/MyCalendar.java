package MyCalendar1;

import java.util.Map;
import java.util.TreeMap;

/**
 * Solution for problem 729: My Calendar 1
 * url: http://leetcode.com/problems/my-calendar-i/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class MyCalendar {

    TreeMap<Integer, Integer> map;

    public MyCalendar() {
        map = new TreeMap<>();
    }

    public boolean book(int start, int end) {
        Map.Entry<Integer, Integer> entry = map.lowerEntry(start);
        if (entry != null && entry.getValue() > start) {
            return false;
        }
        Integer num = map.ceilingKey(start);
        if (num != null && num < end) {
            return false;
        }
        map.put(start, end);
        return true;
    }
}
