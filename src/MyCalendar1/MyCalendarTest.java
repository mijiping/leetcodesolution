package MyCalendar1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class MyCalendarTest {

    @Test
    public void test() {
        MyCalendar obj = new MyCalendar();
        assertAll(
                () -> assertEquals(true, obj.book(10, 20)),
                () -> assertEquals(false, obj.book(15, 25)),
                () -> assertEquals(true, obj.book(30, 35))
        );

    }
}
