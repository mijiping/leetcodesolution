package NextGreatElement1;

import java.util.HashMap;
import java.util.Stack;

/**
 * Solution for problem 496: next greater element I
 * url: http://leetcode.com/problems/next-greater-element-i/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public int[] nextGreaterElement(int[] nums1, int[] nums2) {
        HashMap<Integer, Integer> map = new HashMap<>();
        Stack<Integer> stack = new Stack<>();
        for (int num : nums2) {
           while (!stack.empty() && stack.peek() < num) {
               map.put(stack.pop(), num);
           }
           stack.push(num);
        }
        for (int i = 0; i < nums1.length; i++) {
            nums1[i] = map.getOrDefault(nums1[i], -1);
        }
        return nums1;
    }
}
