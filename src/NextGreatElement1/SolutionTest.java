package NextGreatElement1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SolutionTest {
    Solution solution;

    @BeforeEach
    public void init() {
        solution = new Solution();
    }

    @Test
    public void test() {
        int[] nums1 = {4,1,2};
        int[] nums2 = {1,3,4,2};
        int[] result = {-1,3,-1};
        assertArrayEquals(result, solution.nextGreaterElement(nums1, nums2));
    }
}
