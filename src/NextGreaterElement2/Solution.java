package NextGreaterElement2;

import java.util.Stack;

/**
 * Solution for problem 503: next greater element II
 * url: http://leetcode.com/problems/next-greater-element-ii/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public int[] nextGreaterElements(int[] nums) {
        if (nums == null || nums.length == 0) {
            return nums;
        }
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> indexStack = new Stack<>();
        int[] result = new int[nums.length];
        for(int i = 0; i < nums.length; i++) {
            while (!stack.empty() && stack.peek() < nums[i]) {
                result[indexStack.pop()] = nums[i];
                stack.pop();
            }
            stack.push(nums[i]);
            indexStack.push(i);
        }

        for (int num : nums) {
            while (!stack.empty() && stack.peek() < num) {
                result[indexStack.pop()] = num;
                stack.pop();
            }
        }
        while (!indexStack.empty()) {
            result[indexStack.pop()] = -1;
        }
        return result;
    }
}

