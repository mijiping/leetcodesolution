package NextGreaterElement2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SolutionTest {

    Solution solution;

    @BeforeEach
    public void init() {
        solution = new Solution();
    }

    @Test
    public void test00() {
        int[] nums = {1,2,1};
        int[] expect = {2,-1,2};
        Assertions.assertArrayEquals(expect, solution.nextGreaterElements(nums));
    }

    @Test
    public void test01() {
        int[] nums = {4,3,2,1};
        int[] expect = {-1,4,4,4};
        Assertions.assertArrayEquals(expect, solution.nextGreaterElements(nums));
    }

    @Test
    public void test02() {
        int[] nums = {100,1,11,1,120,111,123,1,-1,-100};
        int[] expect = {120,11,120,120,123,123,-1,100,100,100};
        Assertions.assertArrayEquals(expect, solution.nextGreaterElements(nums));
    }

    @Test
    public void test03() {
        int[] nums = {};
        int[] expect = {};
        Assertions.assertArrayEquals(expect, solution.nextGreaterElements(nums));
    }
}
