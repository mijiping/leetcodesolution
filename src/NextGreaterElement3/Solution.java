package NextGreaterElement3;

import java.util.Arrays;

/**
 * Solution for problem 556: next greater element III
 * url: http://leetcode.com/problems/next-greater-element-iii/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    public int nextGreaterElement(int n) {
        char[] digits = (n + "").toCharArray();
        int length = digits.length;
        boolean isFound = false;
        int number = digits[length - 1];
        int index = length - 2;
        for (; index >= 0; --index) {
            if (number > digits[index]) {
                isFound = true;
                break;
            }
            number = digits[index];
        }
        if (isFound) {
            for (int i = length - 1; i > index; --i) {
                if (digits[i] > digits[index]) {
                    char tmp = digits[i];
                    digits[i] = digits[index];
                    digits[index] = tmp;
                    break;
                }
            }
        }
        Arrays.sort(digits, index + 1, length);
        int result = (int)Long.parseLong(new String(digits));
        return result <= n ? -1 : result;
    }
}
