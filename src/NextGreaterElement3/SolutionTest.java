package NextGreaterElement3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SolutionTest {

    Solution solution;

    @BeforeEach
    public void init(){
        solution = new Solution();
    }

    @Test
    public void test() {
        Assertions.assertAll(
                () -> Assertions.assertEquals(-1, solution.nextGreaterElement(0)),
                () -> Assertions.assertEquals(-1, solution.nextGreaterElement(4321)),
                () -> Assertions.assertEquals(1243, solution.nextGreaterElement(1234)),
                () -> Assertions.assertEquals(60125, solution.nextGreaterElement(56210)),
                () -> Assertions.assertEquals(13222344, solution.nextGreaterElement(12443322)),
                () -> Assertions.assertEquals(-1, solution.nextGreaterElement(1999999999)),
                () -> Assertions.assertEquals(-1, solution.nextGreaterElement(111111))
        );
    }
}
