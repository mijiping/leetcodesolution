package PeakIndexInMountainArray;

/**
 * Solution for problem 852: Peak Index in a Mountain Array
 * url: https://leetcode.com/problems/peak-index-in-a-mountain-array/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    public int peakIndexInMountainArray(int[] A) {
        int l = 0, r = A.length - 1;
        while (l < r) {
            int m = (l + r) / 2;
            if (A[m] > A[m-1]) {
                l = m + 1;
            } else {
                r = m;
            }
        }
        return l - 1;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        int[] A = {1,2,1};
        System.out.println(s.peakIndexInMountainArray(A));
        int[] B = {0,1,7,6,5,4,3};
        System.out.println(s.peakIndexInMountainArray(A));
        int[] C = {0,1,2,3,4,2,1};
        System.out.println(s.peakIndexInMountainArray(C));
    }
}
