package RangeSumQuery;

/**
 * Better solution for problem 303: Range Sum Query - Immutable
 * url: http://leetcode.com/problems/range-sum-query-immutable/
 *
 * @author mijiping
 * @version 2.0.0
 * @since 1.0.0
 */
public class NumArray {

    private int[] sums;

    public NumArray(int[] nums) {
        int length = nums.length;
        sums = new int[length+1];
        for (int i = 0; i < length; ++i) {
            sums[i + 1] = nums[i] + sums[i];
        }
    }

    public int sumRange(int i, int j) {
        return sums[j + 1] - sums[i];
    }
}
