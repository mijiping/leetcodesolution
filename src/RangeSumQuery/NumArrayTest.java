package RangeSumQuery;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class NumArrayTest {

    @Test
    public void test() {
        int[] array = {1,2,3,4,5};
        NumArray obj = new NumArray(array);
        assertAll(
                () -> assertEquals(1, obj.sumRange(0,0)),
                () -> assertEquals(6, obj.sumRange(0,2)),
                () -> assertEquals(5, obj.sumRange(4,4)),
                () -> assertEquals(9, obj.sumRange(3,4)),
                () -> assertEquals(12, obj.sumRange(2,4)),
                () -> assertEquals(15, obj.sumRange(0,4))
        );
    }


}
