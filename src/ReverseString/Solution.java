package ReverseString;

/**
 * Solution for problem 344: Reverse String
 * url: https://leetcode.com/problems/reverse-string/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public void reverseString(char[] s) {
        int l = 0, r = s.length - 1;
        char tmp;
        while (l < r) {
            tmp = s[l];
            s[l] = s[r];
            s[r] = tmp;
            l++;
            r--;
        }
    }
}
