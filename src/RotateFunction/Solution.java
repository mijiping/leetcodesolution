package RotateFunction;

/**
 * Solution for problem 396: Rotate Function
 * url: http://leetcode.com/problems/rotate-function/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    public int maxRotateFunction(int[] A) {
        if (A == null || A.length < 1) {
            return 0;
        }
        int result = Integer.MIN_VALUE;
        int length = A.length;
        int sum;
        for (int m = 0; m < length; m++) {
            sum = 0;
            for (int i = 0, j = m; i < length; i++, j++) {
                sum += ( i * A[j%length]);
            }
            result = Integer.max(result, sum);
        }
        return result;
    }
}
