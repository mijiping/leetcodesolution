package SumOfLeftLeaves;

import util.TreeNode;

import java.util.Stack;

/**
 * Recursive solution for problem 404: Sum od Left Leaves
 * url: http://leetcode.com/problems/sum-of-left-leaves/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public int sumOfLeftLeaves(TreeNode root) {
        int sum = 0;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode element = root;
        while (!stack.isEmpty() || element != null) {
            int count = 0;
            while (element != null) {
                count++;
                stack.push(element);
                element = element.left;
            }
            element = stack.pop();
            if (count > 1 && element.right == null) {
                sum += element.val;
            }
            element = element.right;
        }
        return sum;
    }
}
