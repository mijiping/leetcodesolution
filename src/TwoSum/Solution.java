package TwoSum;

import java.util.HashMap;

/**
 * Solution for problem 1: Two Sum
 * url: https://leetcode.com/problems/two-sum/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int index = map.getOrDefault(nums[i], -1);
            if (index != -1) {
                return new int[] {index, i};
            }
            map.put(target-nums[i], i);
        }
        return null;
    }
}
