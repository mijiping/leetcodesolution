package circularqueue;

/**
 *  Problem: Design Circular Queue
 *  url: leetcode.com/problems/design-circular-queue/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class MyCircularQueue {

    private int[] queue;
    int size, start, end;

    /** Initialize your data structure here. Set the size of the queue to be k. */
    public MyCircularQueue(int k) {
        queue = new int[k+1];
        size = k+1;
        start = 0;
        end = 0;
    }

    /** Insert an element into the circular queue. Return true if the operation is successful. */
    public boolean enQueue(int value) {
        if (isFull()) {
            return false;
        }
        queue[end] = value;
        end = (end + 1) % size;
        return true;
    }

    /** Delete an element from the circular queue. Return true if the operation is successful. */
    public boolean deQueue() {
        if (isEmpty()) {
            return false;
        }
        start = (start + 1) % size;
        return true;
    }

    /** Get the front item from the queue. */
    public int Front() {
        if (isEmpty()) {
            return -1;
        }
        return queue[start];
    }

    /** Get the last item from the queue. */
    public int Rear() {
        if (isEmpty()) {
            return -1;
        }
        int index = (end - 1 + size) % size;
        return queue[index];
    }

    /** Checks whether the circular queue is empty or not. */
    public boolean isEmpty() {
        return start == end;
    }

    /** Checks whether the circular queue is full or not. */
    public boolean isFull() {
        return (end + 1) % size == start;
    }
}