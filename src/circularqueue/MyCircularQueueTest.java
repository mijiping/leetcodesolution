package circularqueue;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class MyCircularQueueTest {
    @Test
    public void queueTest() {
        // 设置长度为3
        MyCircularQueue circularQueue = new MyCircularQueue(3);
        // 返回true
        assertTrue(circularQueue.enQueue(1));
        // 返回true
        assertTrue(circularQueue.enQueue(2));
        // 返回true
        assertTrue(circularQueue.enQueue(3));
        // 返回false,队列已满
        assertFalse(circularQueue.enQueue(4));
        // 返回3
        assertEquals(3,circularQueue.Rear());
        // 返回true
        assertTrue(circularQueue.isFull());
        // 返回true
        assertTrue(circularQueue.deQueue());
        // 返回true
        assertTrue(circularQueue.enQueue(4));
        // 返回4
        assertEquals(4,circularQueue.Rear());
    }
}
