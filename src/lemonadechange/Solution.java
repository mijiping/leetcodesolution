package lemonadechange;

/**
 * Solution for problem 860：Lemonade Change
 * url: leetcode.com/problems/lemonade-change/
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class Solution {
    public boolean lemonadeChange(int[] bills) {
        int numOfFive = 0;
        int numOfTen = 0;
        for(int bill : bills) {
            switch (bill) {
                case 5:
                    ++numOfFive;
                    break;
                case 10:
                    --numOfFive;
                    ++numOfTen;
                    break;
                case 20:
                    if (numOfTen > 0) {
                        --numOfFive;
                        --numOfTen;
                    } else {
                        numOfFive -= 3;
                    }
                    break;
            }
            if (numOfFive < 0 || numOfTen < 0) {
                return false;
            }
        }
        return true;
    }
}
