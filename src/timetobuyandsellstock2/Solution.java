package timetobuyandsellstock2;

/**
 *  Solution for problem 122: Best Time to Buy and Sell Stock
 *  url: http://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
class Solution {
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length < 2) {
            return 0;
        }
        int lastPrice = Integer.MIN_VALUE;
        int sum = 0;
        int min = prices[0];
        for (int current : prices) {
            if (current < lastPrice) {
                if (lastPrice > min) {
                    sum += (lastPrice - min);
                }
                min = current;
            }
            lastPrice = current;
        }
        if (prices[prices.length-1] > min) {
            sum += (prices[prices.length-1] - min);
        }
        return sum;
    }
}
