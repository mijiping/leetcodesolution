package timetobuyandsellstock2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */
public class SolutionTest {

    Solution solution;

    @BeforeEach
    public void init(){
        solution = new Solution();
    }

    @Test
    public void test(){
        int[] prices = {1,2,3,4,5,6};
        assertEquals(5, solution.maxProfit(prices));
    }

    @Test
    public void test01() {
        int[] prices = {1,3,4,5,6,2};
        assertEquals(5, solution.maxProfit(prices));
    }
}
