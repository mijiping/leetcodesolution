package util;

/**
 * Definition for a binary tree node.
 *
 * @author mijiping
 * @version 1.0.0
 * @since 1.0.0
 */

public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int x) {
        val = x;
    }
}
